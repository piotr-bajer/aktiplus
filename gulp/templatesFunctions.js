'use strict';

const path = require('path');
const fs = require('fs');

module.exports = function templateFunctions(data = {}) {
  const properPath = p => (!data.manifest ? p : p.replace(/\\/g, '/'));
  //const properPath = p => p;

  const webpackManifestPath = properPath(
    path.join(
      data.config.dest.base,
      data.config.dest.scripts,
      `manifest${!data.manifest ? '-dev' : ''}.json`
    )
  );

  return [
    {
      name: 'revisionedPath',
      func(fullPath) {
        const pathToFile = path.basename(fullPath);
        if (data.manifest) {
          if (!data.manifest[pathToFile]) {
            throw new Error(`File ${pathToFile} seems to not be revisioned`);
          }
          return properPath(
            path.join(path.dirname(fullPath), data.manifest[pathToFile])
          );
        }

        return fullPath;
      },
    },
    {
      name: 'assetPath',
      func(assetPath) {
        path.sep = '/';
        return properPath(
          path.join(data.config.dest.assets, assetPath).replace(/\\/g, '/')
        );
      },
    },
    {
      name: 'className',
      func(...args) {
        const name = args.shift();
        if (typeof name !== 'string' || name === '') {
          return '';
        }
        const classes = [name];
        let el;
        for (let i = 0; i < args.length; i += 1) {
          el = args[i];
          if (el && typeof el === 'string') {
            classes.push(`${name}--${el}`);
          }
        }
        return classes.join(' ');
      },
    },
    {
      name: 'hasVendor',
      func() {
        if (!data.manifest) {
          return fs.existsSync(
            path.join(
              data.config.dest.base,
              data.config.dest.scripts,
              'vendor.js'
            )
          );
        }

        return !!data.manifest['vendor.js'];
      },
    },
    {
      name: 'getScriptsPath',
      func() {
        return 'scripts/';
      },
    },
    {
      name: 'hasWebpackManifest',
      func() {
        return fs.existsSync(webpackManifestPath);
      },
    },
    {
      name: 'getWebpackManifest',
      func() {
        return fs.readFileSync(webpackManifestPath, 'utf8');
      },
    },

    {
      name: 'emptySrc',
      func() {
        return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAABCAQAAABeK7cBAAAAC0lEQVR42mNkAAIAAAoAAv/lxKUAAAAASUVORK5CYII=';
      },
    },
    /* eslint-disable */
    {
      name: 'lazyImg',
      func(attributes = {}) {
        if (attributes.alt === undefined) {
          attributes.alt = '';
        }

        const tag = attributes.tag ? attributes.tag : 'img';

        if (attributes.tag !== undefined) {
          delete attributes.tag;
        }

        if (attributes.src !== undefined) {
          attributes['data-src'] = attributes.src;
          delete attributes.src;
        }

        if (attributes.srcset !== undefined) {
          attributes['data-srcset'] = attributes.srcset;
          delete attributes.srcset;
        }

        if (attributes.class === undefined) {
          attributes.class = '';
        }

        attributes.class += ' js-lazy-img';

        if (attributes._keys !== undefined) {
          delete attributes._keys;
        }

        const keys = Object.keys(attributes);

        const newAttributes = keys.map(
          attributeKey => `${attributeKey}="${attributes[attributeKey]}"`
        );
        return `<${tag} src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAABCAQAAABeK7cBAAAAC0lEQVR42mNkAAIAAAoAAv/lxKUAAAAASUVORK5CYII=" ${newAttributes.join(
          ' '
        )}>`;
      },
    },
    /* eslint-enable */
  ];
};
