/*
  Project: Aktiplus
  Author: Piotr Bajer
 */

import 'whatwg-fetch';
import 'url-search-params-polyfill';
import smoothscroll from 'smoothscroll-polyfill';
import fix100vh from './fixes/100vh';
import objectFit from './polyfills/object-fit';
import composedPath from './polyfills/composed-path';
import closest from './polyfills/closest';
import lazyLoad from './modules/lazy-load';
import facts from './modules/facts';
import links from './modules/links';
import hitGame from './modules/hit-game';
import header from './modules/header';

smoothscroll.polyfill();
fix100vh();
objectFit();
composedPath();
closest();
lazyLoad();
facts();
links();
hitGame();
header();
