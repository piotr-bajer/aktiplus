import shared from '../modules/shared';
import { on } from '../helpers/shortcuts';

export default () => {
  let orientationChanged = true;
  let refreshTimout = null;

  const updateHeight = () => {
    clearTimeout(refreshTimout);
    refreshTimout = setTimeout(() => {
      if (shared.isMobile) {
        if (orientationChanged) {
          shared.initialHeight = window.innerHeight;
          document.documentElement.style.setProperty(
            '--initial-height',
            `${shared.initialHeight}px`,
          );

          orientationChanged = false;
        }

        document.documentElement.style.setProperty(
          '--full-height',
          `${window.innerHeight}px`,
        );
      }
    }, 200);
  };

  updateHeight();

  window.addEventListener('orientationchange', () => {
    orientationChanged = true;
  });

  on('resize', window, updateHeight);
};
