import { on } from '../helpers/shortcuts';
import { bacteriaData, bacteriaHtmlTemplate } from './hit-game/bacteria-data';
import shared from './shared';

export default () => {
  const gameContainer = document.querySelector('.c-hit-game');

  if (gameContainer === null) {
    return;
  }

  const MAX_TIME = 9 * 60 + 59;
  const START_TIME = 30;
  const MAX_BACTERIA = 14;
  const BACTERIA_RATIO = 0.3;
  const SCREEN_OFFSET = 50;
  const GOOD_BACTERIA = 4;
  const BAD_BACTERIA = 7;
  const INITIAL_TIMER_INTERVAL = 1000;
  const INITIAL_SPEED = 3 * 0.7;
  const IMMUNE_MIN_TIME = 400;
  const IMMUNE_MAX_TIME = 1000;
  const BACTERIA_HIT_TOLERANCE = 10;
  let currentScreen = 'start';
  let currentTime = START_TIME;
  let currentScore = 0;
  let screenWidth = Math.max(1200, shared.windowWidth);
  let screenHeight = 0;
  let minSpeed = INITIAL_SPEED;
  const speedDifference = 0.5;
  let timer = null;
  let timerInterval = INITIAL_TIMER_INTERVAL;
  let rafHandle = null;
  let bacterium = [];
  let usingTouch = false;

  const introContainer = gameContainer.querySelector('.c-hit-game__intro');
  const scoreboardContainer = gameContainer.querySelector(
    '.c-hit-game__scoreboard',
  );
  const timeBoardContainer = gameContainer.querySelector(
    '.c-hit-game__scoreboard-board--time',
  );
  const timeContainer = timeBoardContainer.querySelector('span');
  const resultBoardContainer = gameContainer.querySelector(
    '.c-hit-game__scoreboard-board--result',
  );
  const resultContainer = resultBoardContainer.querySelector('span');
  const startScreenContainer = gameContainer.querySelector(
    '.c-hit-game__start',
  );
  const startGameButton = startScreenContainer.querySelector(
    '.c-hit-game__start-button',
  );
  const gameBoardContainer = gameContainer.querySelector('.c-hit-game__game');
  const finishScreenContainer = gameContainer.querySelector(
    '.c-hit-game__finish',
  );
  const retryGameButton = finishScreenContainer.querySelector(
    '.c-hit-game__finish-retry',
  );
  const hurtContainer = gameContainer.querySelector('.c-hit-game__game-hurt');

  const hideElement = el => {
    el.classList.add('is-hidden');
    el.classList.remove('is-active');
  };

  const showElement = el => {
    el.classList.remove('is-hidden');
    el.classList.add('is-active');
  };

  const randomNumer = (min, max) =>
    min + Math.round((max - min) * Math.random());

  const randomizeSpeed = () => {
    const speed =
      shared.windowWidth < shared.breakpoints.large
        ? minSpeed
        : (100 / 70) * minSpeed;
    return randomNumer(speed, speed + speed * speedDifference);
  };

  const randomizeDirection = () => {
    let d = 0;

    do {
      d = Math.floor(Math.random() * 360);
    } while (d % 90 !== 0);

    const r = (d * 180) / Math.PI;

    return {
      directionX: Math.sin(r) * randomizeSpeed(),
      directionY: Math.cos(r) * randomizeSpeed(),
      rotation: r,
    };
  };

  const bounceChildren = (root, callback, animation = 'bounceOut') => {
    let timeout = 0;

    [...root.children].forEach(el => {
      const animationTime = randomNumer(450, 650);
      const delay = randomNumer(0, 300);

      timeout = Math.max(timeout, delay + animationTime);

      // eslint-disable-next-line no-param-reassign
      el.style.animation = `${animationTime}ms ${delay}ms ${animation} both`;
    });

    return setTimeout(() => {
      [...root.children].forEach(el => {
        // eslint-disable-next-line no-param-reassign
        el.style.animation = '';
      });
      if (callback) {
        callback();
      }
    }, timeout);
  };

  const getBacteriaHeight = () =>
    shared.windowWidth < shared.breakpoints.large ? 70 : 100;

  // eslint-disable-next-line no-unused-vars
  const showIntro = callback => {
    showElement(introContainer);
    return callback && callback();
  };
  const hideIntro = callback => {
    const animationTime = 450;
    introContainer.style.animation = `${animationTime}ms bounceOut both`;

    return setTimeout(() => {
      hideElement(introContainer);
      introContainer.style.animation = '';

      if (callback) {
        callback();
      }
    }, animationTime);
  };

  const showScoreboard = callback => {
    showElement(scoreboardContainer);
    bounceChildren(scoreboardContainer, callback, 'bounceIn');
  };

  // eslint-disable-next-line no-unused-vars
  const hideScoreboard = callback =>
    bounceChildren(scoreboardContainer, () => {
      hideElement(scoreboardContainer);
      if (callback) {
        callback();
      }
    });

  // eslint-disable-next-line no-unused-vars
  const showStartScreen = callback => {
    showElement(startScreenContainer);
    bounceChildren(startScreenContainer, callback, 'bounceIn');
  };
  const hideStartScreen = callback =>
    bounceChildren(startScreenContainer, () => {
      hideElement(startScreenContainer);
      if (callback) {
        callback();
      }
    });

  const showGameBoard = callback => {
    showElement(gameBoardContainer);
    return callback && callback();
  };
  const hideGameBoard = callback => {
    hideElement(gameBoardContainer);
    return callback && callback();
  };

  const showFinishScreen = callback => {
    showElement(finishScreenContainer);
    bounceChildren(finishScreenContainer, callback, 'bounceIn');
  };
  const hideFinishScreen = callback =>
    bounceChildren(finishScreenContainer, () => {
      hideElement(finishScreenContainer);
      if (callback) {
        callback();
      }
    });

  const setTime = (time = 0, animate = false) => {
    const parsedTime = Math.min(MAX_TIME, time);
    const minutes = Math.floor(parsedTime / 60);
    const seconds = Math.floor(parsedTime % 60);

    timeContainer.textContent = `${minutes}:${
      seconds < 10 ? '0' : ''
    }${seconds}`;

    if (animate) {
      timeBoardContainer.style.animation = '';
      requestAnimationFrame(() => {
        timeBoardContainer.style.animation = `300ms scoreBounce both`;
      });
    }
  };

  const endGame = () => {
    if (currentScreen !== 'game') {
      return;
    }

    cancelAnimationFrame(rafHandle);
    currentTime = 0;
    setTime(currentTime);
    hideGameBoard(() => {
      showFinishScreen(() => {
        currentScreen = 'finish';
      });
    });
    currentScreen = '';
  };

  const setTimer = (animate = false) => {
    clearTimeout(timer);

    if (currentTime <= 0) {
      endGame();
      return;
    }

    setTime(currentTime, animate);

    timer = setTimeout(() => {
      currentTime -= 1;
      setTimer();
    }, timerInterval);
  };

  const getImmuneTime = () =>
    new Date().getTime() + randomNumer(IMMUNE_MIN_TIME, IMMUNE_MAX_TIME);

  const setResult = (score, animate = false) => {
    resultContainer.textContent = score;

    if (animate) {
      resultBoardContainer.style.animation = '';
      requestAnimationFrame(() => {
        resultBoardContainer.style.animation = `300ms scoreBounce both`;
      });
    }
  };

  const updateResult = (animate = false) => {
    setResult(currentScore, animate);
  };

  const updateScreenSize = () => {
    const rect = gameBoardContainer.getBoundingClientRect();
    screenWidth = rect.width;
    screenHeight = rect.height;
  };

  const updateBacteriaPos = bacteria => {
    // eslint-disable-next-line no-param-reassign
    bacteria.el.style.transform = `translate(${bacteria.x}px, ${bacteria.y}px)`;
  };

  const getBacteriaSize = bacteria => ({
    height: getBacteriaHeight(),
    width: bacteria.data.ratio * getBacteriaHeight(),
  });

  const createBacteria = (type, number) => {
    const bacteria = {
      type,
      number,
      data: bacteriaData[`${type}${number}`],
      x: Math.random() * (screenWidth - 10),
      y: Math.random() * (screenHeight - 10),
      immune: getImmuneTime(),
      ...randomizeDirection(),
    };

    return { ...bacteria, ...getBacteriaSize(bacteria) };
  };

  const deleteBacteria = bacteria => {
    const newBacterias = [];
    const animationTime = randomNumer(350, 450);

    for (let i = 0; i < bacterium.length; i += 1) {
      if (bacterium[i] === bacteria) {
        // eslint-disable-next-line no-param-reassign
        bacteria.el.firstElementChild.style.animation = `${animationTime}ms bounceOut both`;
        setTimeout(() => {
          requestAnimationFrame(() => {
            bacteria.el.parentNode.removeChild(bacteria.el);
          });
        }, animationTime);
      } else {
        newBacterias.push(bacterium[i]);
      }
    }

    bacterium = newBacterias;
  };

  const addBacteria = () => {
    let goodBacterium = 0;

    bacterium.forEach(bacteria => {
      if (bacteria.type === 'good') {
        goodBacterium += 1;
      }
    });

    const type = goodBacterium > BACTERIA_RATIO * MAX_BACTERIA ? 'bad' : 'good';
    const number = randomNumer(
      1,
      type === 'good' ? GOOD_BACTERIA : BAD_BACTERIA,
    );
    const bacteria = createBacteria(type, number);

    const animationTime = randomNumer(450, 650);

    gameBoardContainer.insertAdjacentHTML(
      'beforeend',
      bacteriaHtmlTemplate(
        type,
        number,
        `animation: ${animationTime}ms 1 bounceIn both`,
      ),
    );

    bacteria.el = gameBoardContainer.lastElementChild;
    updateBacteriaPos(bacteria);
    bacterium.push(bacteria);
  };

  const onUpdate = () => {
    if (currentScreen !== 'game') {
      return;
    }

    const time = new Date().getTime();

    for (let i = 0; i < bacterium.length; i += 1) {
      const bacteria = bacterium[i];

      if (
        bacteria.x < -SCREEN_OFFSET ||
        bacteria.x > screenWidth + SCREEN_OFFSET
      ) {
        bacteria.directionX *= -1;
      }

      if (
        bacteria.y < -SCREEN_OFFSET ||
        bacteria.y > screenHeight + SCREEN_OFFSET
      ) {
        bacteria.directionY *= -1;
      }

      if (time > bacteria.immune) {
        for (let ii = 0; ii < bacterium.length; ii += 1) {
          const bounceBacteria = bacterium[ii];

          if (i !== ii && time > bounceBacteria.immune) {
            const xdist = Math.abs(bounceBacteria.x - bacteria.x);
            const ydist = Math.abs(bounceBacteria.y - bacteria.y);
            const dist = Math.sqrt(xdist * xdist + ydist * ydist);
            if (
              dist < (bounceBacteria.width + bacteria.width) * 0.5 ||
              dist < (bounceBacteria.height + bacteria.height) * 0.5
            ) {
              bacterium[i] = {
                ...bacteria,
                ...randomizeDirection(bacteria),
                immune: getImmuneTime(),
              };
              bacterium[ii] = {
                ...bounceBacteria,
                ...randomizeDirection(bounceBacteria),
                immune: getImmuneTime(),
              };
            }
          }
        }
      }

      bacteria.x -= bacteria.directionX;
      bacteria.y -= bacteria.directionY;

      updateBacteriaPos(bacteria);
    }

    rafHandle = requestAnimationFrame(onUpdate);
  };

  const initGame = () => {
    let bacteriumHTML = '';
    bacterium = [];

    const bacteriumElements = gameBoardContainer.querySelectorAll(
      '.c-hit-game__bacteria',
    );
    [...bacteriumElements].forEach(el => el.parentNode.removeChild(el));

    for (let i = 1; i <= MAX_BACTERIA; i += 1) {
      const type = i > BACTERIA_RATIO * MAX_BACTERIA ? 'bad' : 'good';
      const number = randomNumer(
        1,
        type === 'good' ? GOOD_BACTERIA : BAD_BACTERIA,
      );
      const bacteria = createBacteria(type, number);
      const animationTime = randomNumer(450, 650);
      const delay = randomNumer(0, 300);

      bacteriumHTML += bacteriaHtmlTemplate(
        type,
        number,
        `animation: ${animationTime}ms ${delay}ms 1 bounceIn both`,
      );

      bacterium.push(bacteria);
    }

    const childrenOffset = gameBoardContainer.children.length;

    gameBoardContainer.insertAdjacentHTML('beforeend', bacteriumHTML);

    for (let i = 0; i < MAX_BACTERIA; i += 1) {
      bacterium[i].el = gameBoardContainer.children[i + childrenOffset];
    }

    onUpdate();
  };

  const startGame = () => {
    if (currentScreen === 'game') {
      return;
    }

    timerInterval = INITIAL_TIMER_INTERVAL;
    currentTime = START_TIME;
    minSpeed = INITIAL_SPEED;
    currentScore = 0;
    setResult(currentScore);
    setTimer();

    if (currentScreen === 'start') {
      hideIntro();
      hideStartScreen(() => {
        showScoreboard();
        showGameBoard(initGame);
      });
    } else if (currentScreen === 'finish') {
      hideFinishScreen(() => {
        showGameBoard(initGame);
      });
    }

    currentScreen = 'game';
  };

  const handleClick = event => {
    if (
      currentScreen !== 'game' ||
      (event.type === 'mousedown' && usingTouch)
    ) {
      return;
    }

    const pos = { x: 0, y: 0 };
    const rect = gameBoardContainer.getBoundingClientRect();

    if (event.type === 'mousedown') {
      pos.x = event.clientX - rect.left;
      pos.y = event.clientY - rect.top;
    } else {
      pos.x = event.touches[0].clientX - rect.left;
      pos.y = event.touches[0].clientY - rect.top;

      if (pos.x < 0 || pos.x > rect.width || pos.y < 0 || pos.y > rect.height) {
        return;
      }
    }

    let hitBacteria = null;
    let currentDistance = -1;

    for (let i = 0; i < bacterium.length; i += 1) {
      const bacteria = bacterium[i];
      const width = bacteria.width * 0.5 + BACTERIA_HIT_TOLERANCE;
      const height = bacteria.height * 0.5 + BACTERIA_HIT_TOLERANCE;

      if (
        pos.x > bacteria.x - width &&
        pos.x < bacteria.x + width &&
        pos.y > bacteria.y - height &&
        pos.y < bacteria.y + height
      ) {
        const xdist = Math.abs(bacteria.x - pos.x);
        const ydist = Math.abs(bacteria.y - pos.y);
        const dist = Math.sqrt(xdist * xdist + ydist * ydist);

        if (currentDistance < 0 || dist < currentDistance) {
          currentDistance = dist;
          hitBacteria = bacteria;
        }
      }
    }

    if (hitBacteria !== null) {
      if (hitBacteria.type === 'good') {
        currentTime -= 2;
        hurtContainer.style.animation = 'none';

        setTimeout(() => {
          hurtContainer.style.animation = '0.45s zoomIn both';
        });
      } else {
        currentScore += 1;
        currentTime += 2;
      }

      timerInterval = Math.max(200, INITIAL_TIMER_INTERVAL - currentScore * 50);

      minSpeed += 0.1;

      setTimer(false);
      updateResult(false);
      deleteBacteria(hitBacteria);
      addBacteria();
    }
  };

  startGameButton.addEventListener('click', startGame);
  retryGameButton.addEventListener('click', () => {
    hideScoreboard();
    hideFinishScreen(() => {
      showStartScreen();
      showIntro();
    });

    currentScreen = 'start';
  });

  updateScreenSize();

  on('resize', window, updateScreenSize);
  on('mousedown', gameContainer, () => {
    usingTouch = false;
  });
  on(
    'touchstart',
    gameContainer,
    () => {
      usingTouch = true;
    },
    shared.hasPassiveEvents ? { passive: true } : undefined,
  );
  on(
    'touchstart',
    gameBoardContainer,
    handleClick,
    shared.hasPassiveEvents ? { passive: true } : undefined,
  );
  on('mousedown', gameBoardContainer, handleClick);
};
