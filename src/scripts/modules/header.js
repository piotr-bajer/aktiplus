import { on } from '../helpers/shortcuts';
import shared from './shared';

export default () => {
  const header = document.querySelector('.c-header');

  if (header === null) {
    return;
  }

  const menuOpen = header.querySelector('.c-header__menu-open');
  const menuClose = header.querySelector('.c-header__menu-close');

  on('click', menuOpen, event => {
    event.preventDefault();
    header.classList.add(shared.CSS.active);
  });

  on('click', menuClose, event => {
    event.preventDefault();
    header.classList.remove(shared.CSS.active);
  });
};
