import { on } from '../helpers/shortcuts';
import shared from './shared';

export default () => {
  const facts = document.querySelector('.c-facts');

  if (facts === null) {
    return;
  }

  const moreButtons = facts.querySelectorAll('.c-facts__more');
  const backButtons = facts.querySelectorAll('.c-facts__back');
  const overlays = facts.querySelectorAll('.c-facts__overlay');
  let activeOverlays = [];
  let skipScroll = false;

  const getElementTop = el => el.getBoundingClientRect().top + shared.scrollTop;

  const isVisibleOnScreen = top =>
    shared.scrollTop < top && shared.scrollTop + shared.windowHeight > top;

  const scrollToElement = el => {
    const top = getElementTop(el);

    if (!isVisibleOnScreen(top)) {
      window.scrollTo({
        top: top - shared.getHeaderHeight(),
        behavior: 'smooth',
      });
    }
  };

  on('click', moreButtons, event => {
    event.preventDefault();
    event.stopPropagation();
    const button = event.currentTarget;
    const overlay = facts.querySelector(`#${button.dataset.trigger}`);

    button.parentNode.classList.add('is-hidden');
    button.parentNode.parentNode.classList.add('is-hidden');
    overlay.classList.add('is-active');
    activeOverlays.push(overlay);

    scrollToElement(overlay);
  });

  on('click', backButtons, event => {
    event.preventDefault();
    const button = event.currentTarget;
    const overlay = facts.querySelector(`#${button.dataset.trigger}`);
    const product = facts.querySelector(
      `.c-facts__product--${button.dataset.trigger}`,
    );

    product.classList.remove('is-hidden');
    product.parentNode.classList.remove('is-hidden');
    overlay.classList.remove('is-active');

    if (!skipScroll) {
      scrollToElement(product);
    }
  });

  on('click', overlays, event => event.stopPropagation());

  on('click', document.documentElement, () => {
    if (activeOverlays.length > 0) {
      skipScroll = true;
      activeOverlays.forEach(overlay => {
        const backButton = overlay.querySelector('.c-facts__back');

        if (backButton !== null) {
          backButton.click();
        }
      });

      activeOverlays = [];
      skipScroll = false;
    }
  });
};
