export const bacteriaHtmlTemplate = (
  type = 'bad',
  number = 1,
  imageStyle = '',
) => `<div class="c-hit-game__bacteria c-hit-game__bacteria--${type} c-hit-game__bacteria--${
  type === 'bad' ? 'b' : 'g'
}${number}">
            <img class="c-hit-game__bacteria-img" alt="" src="assets/images/${type}-bacteria-${number}.svg" style="${imageStyle}">
          </div>`;

export const bacteriaData = {
  bad1: {
    ratio: 14 / 24,
  },
  bad2: {
    ratio: 16 / 25,
  },
  bad3: {
    ratio: 25 / 19,
  },
  bad4: {
    ratio: 23 / 24,
  },
  bad5: {
    ratio: 22 / 24,
  },
  bad6: {
    ratio: 22 / 23,
  },
  bad7: {
    ratio: 5 / 19,
  },
  good1: {
    ratio: 22 / 23,
  },
  good2: {
    ratio: 22 / 24,
  },
  good3: {
    ratio: 23 / 22,
  },
  good4: {
    ratio: 5 / 19,
  },
};
