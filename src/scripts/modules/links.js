import { on } from '../helpers/shortcuts';

export default () => {
  const links = document.querySelectorAll('[href^="#"]');

  if (links.length < 1) {
    return;
  }

  on('click', links, event => {
    const href = event.currentTarget.getAttribute('href').replace('#', '');

    if (href.length < 1) {
      return;
    }

    const target = document.querySelector(`#${href}`);

    if (target !== null) {
      event.preventDefault();
      target.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  });
};
